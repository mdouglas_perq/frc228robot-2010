/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway;

import edu.wpi.first.wpilibj.camera.*;
import edu.wpi.first.wpilibj.image.*;
import edu.wpi.first.wpilibj.samples.Target;
import edu.wpi.first.wpilibj.samples.TrackerDashboard;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.Timer;

import org.team228.frc.breakaway.util.Config;
import org.team228.frc.breakaway.mechanisms.SwerveDrive;

/**
 *
 */
public class VisionTracker
{
    private class TrackerThread extends Thread
    {
        private VisionTracker tracker;

        public TrackerThread(VisionTracker t)
        {
            tracker = t;
        }
        
        public void run()
        {
            //AxisCamera cam = AxisCamera.getInstance();

            // Start tracker dashboard
            //TrackerDashboard trackerDashboard = new TrackerDashboard();

            /*
            while (true)
            {
                if (tracker.isEnabled())
                {
                    try
                    {
                        if (cam.freshImage())
                        {
                            ColorImage image = cam.getImage();
                            Thread.yield();
                            Target[] targets = Target.findCircularTargets(image);
                            Thread.yield();
                            image.free();

                            if (targets.length == 0 || targets[0].m_score < 0.01)
                            {
                                // no matches found
                                //System.out.println("No target found");
                                Target[] newTargets = new Target[targets.length + 1];

                                newTargets[0] = new Target();
                                newTargets[0].m_majorRadius = 0;
                                newTargets[0].m_minorRadius = 0;
                                newTargets[0].m_score = 0;
                                for (int i = 0; i < targets.length; i++)
                                {
                                    newTargets[i + 1] = targets[i];
                                }

                                tracker.setTarget(null);

                                // Update default vision dashboard
                                trackerDashboard.updateVisionDashboard(0.0, SwerveDrive.getInstance().getDirection(), 0.0, 0.0, newTargets);
                            }
                            else
                            {
                                System.out.println(targets[0]);
                                System.out.println("Target Angle: " + targets[0].getHorizontalAngle());

                                // Report target information
                                tracker.setTarget(targets[0]);

                                // Update default vision dashboard
                                trackerDashboard.updateVisionDashboard(0.0, SwerveDrive.getInstance().getDirection(), 0.0, targets[0].m_xPos / targets[0].m_xMax, targets);
                            }
                        }
                    }
                    catch (NIVisionException ex)
                    {
                        ex.printStackTrace();
                    }
                    catch (AxisCameraException ex)
                    {
                        ex.printStackTrace();
                    }

                    Timer.delay(0.2);
                }
            }
             *
             */
        }
    }
    
    private static volatile VisionTracker instance = new VisionTracker();

    private AxisCamera camera;
    private Servo servo;
    private Thread task;
    private volatile boolean enabled = false;

    private volatile Target target = null;

    private double servoHighPos = 0.0;
    private double servoLowPos = 1.0;

    public static VisionTracker getInstance()
    {
        if (instance == null)
        {
            synchronized (VisionTracker.class)
            {
                if (instance == null)
                {
                    instance = new VisionTracker();
                }
            }
        }

        return instance;
    }

    private VisionTracker()
    {
        /*
        //Create camera instance
        camera = AxisCamera.getInstance();

        // Camera configuration
        camera.writeResolution(AxisCamera.ResolutionT.k320x240);
        camera.writeBrightness(0);
        camera.writeCompression(30);
         *
         */

        // Camera servo
        servo = new Servo(Config.getInt("camera.servo.pwm", 2));
        servo.setBounds(254, 0, 0, 0, 1);
        servoHighPos = Config.getDouble("camera.servo.high", 0.0);
        servoLowPos = Config.getDouble("camera.servo.low", 1.0);

        // Start the tracker task
        //task = new TrackerThread(this);
        //task.start();
    }

    /**
     * Disable vision tracking.
     */
    public void disable()
    {
        enabled = false;
    }

    /**
     * Enable vision tracking.
     */
    public void enable()
    {
        enabled = true;
    }

    /**
     * Determine if the target has been located.
     * @return  <code>true</code> if the target is found,
     *          <code>false</code> otherwise.
     */
    private boolean foundTarget()
    {
        return target != null;
    }

    /**
     * Get the last target found.
     *
     * @return  Target information.
     */
    public Target getTarget()
    {
        return target;
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    /**
     * Pan the camera servo to face the High side of the robot.
     */
    public void setServoHigh()
    {
        servo.set(servoHighPos);
    }

    /**
     * Pan the camera servo to face the Low side of the robot.
     */
    public void setServoLow()
    {
        servo.set(servoLowPos);
    }

    /**
     * Store tracking information for retrieval.
     * @param t     Target tracking information.
     */
    private void setTarget(Target t)
    {
        target = t;
    }
}
