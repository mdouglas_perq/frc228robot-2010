/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/
package org.team228.frc.breakaway.mechanisms;

import edu.wpi.first.wpilibj.Dashboard;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.Victor;
import edu.wpi.first.wpilibj.Watchdog;

import org.team228.frc.breakaway.util.Config;
import org.team228.frc.breakaway.dashboard.DashboardDataProvider;
import org.team228.frc.breakaway.VisionTracker;

/**
 * This class implements the control for the kicker mechanisms.
 */
public class Kicker implements Operatable, DashboardDataProvider
{

    /**
     * The arming mode.
     */
    public static class Mode
    {

        /**
         * The integer value representing this enumeration
         */
        public final int value;
        static final int kLowPower_val = 0;
        static final int kHighPower_val = 1;
        static final int kNeutral_val = 2;

        /**
         * The kicker is in high power mode
         */
        public static final Mode kHighPower = new Mode(kHighPower_val);
        /**
         * The kicker is in low power mode
         */
        public static final Mode kLowPower = new Mode(kLowPower_val);
        /**
         * The kicker is in neutral mode
         */
        public static final Mode kNeutral = new Mode(kNeutral_val);

        private Mode(int value)
        {
            this.value = value;
        }
    }

    /**
     * The kicker state.
     */
    public static class State
    {
        /**
         * The integer value representing this enumeration
         */
        public final int value;
        static final int kIdle_val = 0;
        static final int kShifting_val = 1;
        static final int kFiring_val = 2;
        /**
         * The kicker is in an idle state.
         */
        public static final State kIdle = new State(kIdle_val);
        /**
         * The kicker is shifting.
         */
        public static final State kShifting = new State(kShifting_val);
        /**
         * The kicker is firing.
         */
        public static final State kFiring = new State(kFiring_val);

        private State(int value)
        {
            this.value = value;
        }
    }
    public final Object semaphore = new Object();

    // Maximum time to run the kicker motor, in seconds
    public final double kKickerTimeout = 4.0;

    // Singleton instance
    private static volatile Kicker instance;

    // State information
    private volatile Mode mode = Mode.kNeutral;
    private volatile State state = State.kIdle;

    // Speed controllers
    private Victor cim;

    // Shifting servo
    private Servo shifter;

    // Sensors
    private DigitalInput highPowerSwitch;
    private DigitalInput lowPowerSwitch;

    // Driver control
    private Joystick gamepad;
    private int shiftHighButton = 1;
    private int fireButton = 3;
    private int shiftLowButton = 3;

    // Button state holder
    private boolean shiftLowOld = false;
    private boolean shiftHighOld = false;
    private boolean fireButtonOld = false;
    private double highSpeed = 1.0;
    private double lowSpeed = -1.0;
    private double shiftSpeed = 0.1;

    // Shifter servo position
    private double shifterHighPos = 1.0;
    private double shifterLowPos = 0.5;

    /**
     * Get the singleton instance of Kicker.
     *
     * @return The singleton Kicker object.
     */
    public static Kicker getInstance()
    {
        if (instance == null)
        {
            synchronized (Kicker.class)
            {
                if (instance == null)
                {
                    instance = new Kicker();
                }
            }
        }

        return instance;
    }

    /**
     * Private constructor.
     * Initializes the Kicker object.
     */
    private Kicker()
    {
        System.out.println("[Kicker] Initializing...");

        // Setup speed controller
        cim = new Victor(Config.getInt("kicker.cim.pwm", 1));

        // CIM speeds
        highSpeed = Config.getDouble("kicker.high.speed", -1.0);
        lowSpeed = Config.getDouble("kicker.low.speed", 1.0);
        shiftSpeed = Config.getDouble("kicker.shifter.speed", 0.3);

        // Joystick configuration
        gamepad = new Joystick(Config.getInt("kicker.joystick", 2));
        shiftHighButton = Config.getInt("kicker.joystick.shifthigh", 1);
        shiftLowButton = Config.getInt("kicker.joystick.shiftlow", 3);
        fireButton = Config.getInt("kicker.joystick.fire", 2);

        // Shifter servo
        shifter = new Servo(Config.getInt("kicker.shifter.pwm", 3));
        shifterLowPos = Config.getDouble("kicker.shifter.low", 0.9);
        shifterHighPos = Config.getDouble("kicker.shifter.high", 0.2);

        // Kicker detection switches
        lowPowerSwitch = new DigitalInput(Config.getInt("kicker.low.dio", 1));
        highPowerSwitch = new DigitalInput(Config.getInt("kicker.high.dio", 3));
    }

    /**
     * Get the arming state of the engaged kicker.
     *
     * @return  <code>true</code> if the current kicker is armed,
     *          <code>false</code> otherwise.
     */
    public boolean isArmed()
    {
        if (mode == Mode.kHighPower)
            return !getHighSwitch();
        else if (mode == Mode.kLowPower)
            return !getLowSwitch();
        return false;
    }

    /**
     * Fires the active kicker.
     * A separate thread is spawned to execute the sequence the background.
     *
     * Note: This method name is somewhat misleading.
     * This will run the kicker up until it hits a limit switch.
     * The result is that it fires and re-arms if it's armed, and if
     * it isn't armed it will only arm it and stop.
     *
     * @todo Come up with a better name
     * @todo Fire without automatically reloading?
     */
    public void fire()
    {
        if (state != State.kIdle || mode == Mode.kNeutral)
        {
            return;
        }

        //Watchdog.getInstance().feed();
        Watchdog.getInstance().setEnabled(false);

        // Start a new thread for this task...
        (new Thread(new Runnable()
        {
            public void run()
            {
                synchronized (semaphore)
                {
                    // set fire state
                    state = State.kFiring;

                    Timer timer = new Timer();

                    // switches are normally open, e.g. HIGH
                    // need to rotate until switch goes HIGH->LOW
                    boolean switchOld = (mode == Mode.kHighPower) ? getHighSwitch() : getLowSwitch();
                    boolean limitSwitch = switchOld;

                    // Turn on motor
                    cim.set((mode == Mode.kHighPower) ? highSpeed : lowSpeed);

                    // Start timer for timeout
                    timer.start();

                    // wait for edge change
                    while (!(switchOld == true && limitSwitch == false) && timer.get() < kKickerTimeout)
                    {
                        Watchdog.getInstance().feed();
                        switchOld = limitSwitch;
                        limitSwitch = (mode == Mode.kHighPower) ? getHighSwitch() : getLowSwitch();
                    }

                    // Turn off motor
                    cim.set(0);

                    // set idle state
                    state = State.kIdle;
                }
            }
        })).start();   // Run the thread

        Watchdog.getInstance().setEnabled(true);
        Watchdog.getInstance().feed();
    }

    /**
     * Get the state of the limit switch on the high side of the kicker.
     * The switch is normally open.
     * @return  <code>true</code> if the switch is open, <code>false</code>
     *          if the switch is closed.
     */
    public boolean getHighSwitch()
    {
        return highPowerSwitch.get();
    }

    /**
     * Get the state of the limit switch on the low side of the kicker.
     * The switch is normally open.
     * @return  <code>true</code> if the switch is open, <code>false</code>
     *          if the switch is closed.
     */
    public boolean getLowSwitch()
    {
        return lowPowerSwitch.get();
    }

    /**
     * Get the operating mode of the kicker.
     * @return  One of three possible Modes:
     *          <code>kLowPower, kNeutral, kHighPower</code>
     */
    public Mode getMode()
    {
        return mode;
    }

    /**
     * Get the state of the kicker.
     * @return  One of three possible States:
     *          <code>kIdle, kShifting, kFiring</code>
     */
    public State getState()
    {
        return state;
    }

    /**
     * Shift to the high power side.
     * A separate thread is spawned to execute the sequence the background.
     */
    public void shiftHigh()
    {
        if (mode == Mode.kHighPower || state != State.kIdle)
        {
            return; // Don't shift while firing or shifting etc.
        }

        // Move camera servo to high position
        VisionTracker.getInstance().setServoHigh();

        Watchdog.getInstance().feed();

        (new Thread(new Runnable()
        {
            public void run()
            {
                synchronized (semaphore)
                {
                    state = State.kShifting;

                    shifter.set(shifterHighPos);
                    Timer.delay(0.5);

                    cim.set(highSpeed * shiftSpeed);
                    Timer.delay(0.3);

                    cim.set(0.0);

                    mode = Mode.kHighPower;
                    state = State.kIdle;
                }
            }
        })).start();

        Watchdog.getInstance().feed();
    }

    /**
     * Shift to the low power side.
     * A separate thread is spawned to execute the sequence the background.
     */
    public void shiftLow()
    {
        if (mode == Mode.kLowPower || state != State.kIdle)
        {
            return; // Don't shift while firing or shifting etc.
        }

        // Move camera servo to low position
        VisionTracker.getInstance().setServoLow();

        Watchdog.getInstance().feed();
        
        (new Thread(new Runnable()
        {
            public void run()
            {
                synchronized (semaphore)
                {
                    state = State.kShifting;

                    shifter.set(shifterLowPos);
                    Timer.delay(0.5);

                    cim.set(lowSpeed * shiftSpeed);
                    Timer.delay(0.3);

                    cim.set(0.0);

                    mode = Mode.kLowPower;
                    state = State.kIdle;
                }
            }
        })).start();

        Watchdog.getInstance().feed();
    }

    public void operateContinuous()
    {
    }

    public void operateInit()
    {
        cim.set(0.0);
    }

    public void operatePeriodic()
    {
        boolean shiftHighVal = gamepad.getRawButton(shiftHighButton);
        boolean shiftLowVal = gamepad.getRawButton(shiftLowButton);
        boolean fireVal = gamepad.getRawButton(fireButton);

        Watchdog.getInstance().feed();

        // rising edge for shifting
        if (shiftHighOld == false && shiftHighVal == true)
        {
            shiftHigh();
        }
        else if (shiftLowOld == false && shiftLowVal == true)
        {
            shiftLow();
        }
        else if (fireButtonOld == false && fireVal == true)
        {
            fire();
        }

        shiftHighOld = shiftHighVal;
        shiftLowOld = shiftLowVal;
        fireButtonOld = fireVal;

        Watchdog.getInstance().feed();
    }

    public void operateExit()
    {
        cim.set(0.0);
    }

    public void packDashboard(Dashboard dash)
    {

    }
}
