/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.mechanisms;

import com.sun.squawk.util.MathUtils;

import edu.wpi.first.wpilibj.Dashboard;
import edu.wpi.first.wpilibj.Gyro;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDSource;
import edu.wpi.first.wpilibj.Watchdog;

import org.team228.frc.breakaway.util.Config;
import org.team228.frc.breakaway.util.GenericPIDOutput;
import org.team228.frc.breakaway.dashboard.DashboardDataProvider;

/**
 * This class implements the swerve drive system.
 * 
 */
public class SwerveDrive implements Operatable, DashboardDataProvider
{
    private static volatile SwerveDrive instance;

    private Gyro gyro;
    private SwerveModule lf, lr, rf, rr;
    private Joystick stick;
    private int gyroResetButton = 9;
    private boolean gyroResetButtonOld = false;
    private double headingOffset = 0.0;

    private PIDController rotatePIDControl;
    private GenericPIDOutput rotatePIDOutput;
    private boolean rotatePIDEnabled = false;


    private final double kJoystickDeadband = 0.05;
    private final double kRotationGain = 0.3;

    /**
     * Get the singleton instance of SwerveDrive.
     *
     * @return The singleton SwerveDrive object.
     */
    public static SwerveDrive getInstance()
    {
        if (instance == null)
        {
            synchronized (SwerveDrive.class)
            {
                // Create the singleton instance if it hasn't been done already
                if (instance == null)
                {
                    instance = new SwerveDrive();
                }
            }
        }

        return instance;
    }

    /**
     * Initializes all of the objects required for the SwerveDrive.
     *
     * @todo Add in default configuration values.
     */
    private SwerveDrive()
    {
        System.out.println("[SwerveDrive] Initializing...");

        // Configure the Gyro
        gyro = new Gyro(Config.getInt("swerve.gyro.aichannel", 1));
        gyro.setSensitivity(Config.getDouble("swerve.gyro.sensitivity", 0.006));

        // Create driver Joystick
        stick = new Joystick(Config.getInt("swerve.joystick", 1));
        gyroResetButton = Config.getInt("swerve.joystick.gyroreset", 9);

        // Setup PID controller
        rotatePIDOutput = new GenericPIDOutput();
        rotatePIDControl = new PIDController(Config.getDouble("swerve.turn.p", 0.03),
                                             Config.getDouble("swerve.turn.i", 0.0),
                                             Config.getDouble("swerve.turn.d", 0.0),
                                             new PIDSource() {
                                                public double pidGet()
                                                {
                                                    return getDirection();
                                                }
                                            },
                                            rotatePIDOutput, 0.02);
        rotatePIDControl.setTolerance(Config.getDouble("swerve.turn.tolerance", 5.0));
        rotatePIDControl.setInputRange(-180.0, 180.0);
        rotatePIDControl.setContinuous(true);
        rotatePIDControl.disable();

        // Initialize Swerve Drive Modules
        lf = new SwerveModule(Config.getInt("swerve.lf.steer.canid", 20),
                              Config.getInt("swerve.lf.drive.canid", 21));
        rf = new SwerveModule(Config.getInt("swerve.rf.steer.canid", 22),
                              Config.getInt("swerve.rf.drive.canid", 23));
        lr = new SwerveModule(Config.getInt("swerve.lr.steer.canid", 26),
                              Config.getInt("swerve.lr.drive.canid", 27));
        rr = new SwerveModule(Config.getInt("swerve.rr.steer.canid", 24),
                              Config.getInt("swerve.rr.drive.canid", 25));

        // Configure the Left Front module
        lf.setInvertedDrive(Config.getBoolean("swerve.lf.invertdrive"));
        lf.setCenter(Config.getDouble("swerve.lf.steer.center", 0.5));
        lf.setSensitivity(Config.getDouble("swerve.lf.steer.sensitivity"));
        lf.setPID(Config.getDouble("swerve.lf.steer.p"),
                  Config.getDouble("swerve.lf.steer.i"),
                  Config.getDouble("swerve.lf.steer.d"));
        lf.setPositionLimits(Config.getDouble("swerve.lf.steer.fwdlimit", 1.0),
                             Config.getDouble("swerve.lf.steer.revlimit", 0.0));

        // Configure the Right Front module
        rf.setInvertedDrive(Config.getBoolean("swerve.rf.invertdrive"));
        rf.setCenter(Config.getDouble("swerve.rf.steer.center"));
        rf.setPID(Config.getDouble("swerve.rf.steer.p"), Config.getDouble("swerve.rf.steer.i"), Config.getDouble("swerve.rf.steer.d"));
        rf.setSensitivity(Config.getDouble("swerve.rf.steer.sensitivity"));
        rf.setPositionLimits(Config.getDouble("swerve.rf.steer.fwdlimit"), Config.getDouble("swerve.rf.steer.revlimit"));

        // Configure the Left Rear module
        lr.setInvertedDrive(Config.getBoolean("swerve.lr.invertdrive"));
        lr.setCenter(Config.getDouble("swerve.lr.steer.center"));
        lr.setPID(Config.getDouble("swerve.lr.steer.p"), Config.getDouble("swerve.lr.steer.i"), Config.getDouble("swerve.lr.steer.d"));
        lr.setSensitivity(Config.getDouble("swerve.lr.steer.sensitivity"));
        lr.setPositionLimits(Config.getDouble("swerve.lr.steer.fwdlimit"), Config.getDouble("swerve.lr.steer.revlimit"));

        // Configure the Right Rear module
        rr.setInvertedDrive(Config.getBoolean("swerve.rr.invertdrive"));
        rr.setCenter(Config.getDouble("swerve.rr.steer.center"));
        rr.setPID(Config.getDouble("swerve.rr.steer.p"), Config.getDouble("swerve.rr.steer.i"), Config.getDouble("swerve.rr.steer.d"));
        rr.setSensitivity(Config.getDouble("swerve.rr.steer.sensitivity"));
        rr.setPositionLimits(Config.getDouble("swerve.rr.steer.fwdlimit"), Config.getDouble("swerve.rr.steer.revlimit"));

        enableSteerControl();
    }

    /**
     * Drive mode for driving over the bumps.
     * All wheel modules are locked in to their center positions.
     * 
     * @param speed Speed of forward/reverse motion of the robot.
     */
    public void bumpDrive(double speed)
    {
        lf.set(speed, 0.0);
        lr.set(speed, 0.0);
        rf.set(speed, 0.0);
        rr.set(speed, 0.0);
    }

    /**
     *
     * Drives the robot using the specifed parameters.
     *
     * @param magnitude The "speed" of translation. This should be
     *                  positive, ranging from 0.0 to 1.0.
     * @param direction The direction of motion. This is represented in degrees.
     *                  Range is -180.0 to +180.0, with 0.0 indicating straight
     *                  ahead. This is intended to be relative direction from
     *                  the driver's point of view.
     * @param rotation  Rotational proportion. -1.0 for full speed CCW,
     *                  0.0 for none, and +1.0 for full speed CW.
     */
    public void drive(double magnitude, double direction, double rotation)
    {
        // This doesn't play nice with negative zero...
        if (magnitude == -0.0)
            magnitude = 0.0;
        if (direction == -0.0)
            direction = 0.0;
        if (rotation == -0.0)
            rotation = 0.0;

        double vtx, vty;        // Translational velocity vectors

        double lfvx, lfvy;      // LF wheel vectors
        double lrvx, lrvy;
        double rfvx, rfvy;
        double rrvx, rrvy;

        double lfv, lrv, rfv, rrv;

        double lftheta;
        double lrtheta;
        double rftheta;
        double rrtheta;

        // Modify direction by current heading
        // This enables driver-relative control
        direction = ((direction - getDirection() + 360.0 + 180.0) % 360.0) - 180.0;
        //System.out.println("[SwerveDrive]: Gyro-Adjusted Direction = " + direction);

        // Get vectors for magntidue and direction
        vtx = magnitude * Math.sin(Math.toRadians(direction));
        vty = magnitude * Math.cos(Math.toRadians(direction));

        // Wheel x scalars
        lfvx = vtx + rotation * kRotationGain;
        lrvx = vtx - rotation * kRotationGain;
        rfvx = vtx + rotation * kRotationGain;
        rrvx = vtx - rotation * kRotationGain;

        // Wheel y scalars
        lfvy = vty + rotation * kRotationGain;
        lrvy = vty + rotation * kRotationGain;
        rfvy = vty - rotation * kRotationGain;
        rrvy = vty - rotation * kRotationGain;

        // Wheel velocities
        lfv = Math.sqrt(lfvx * lfvx + lfvy * lfvy);
        lrv = Math.sqrt(lrvx * lrvx + lrvy * lrvy);
        rfv = Math.sqrt(rfvx * rfvx + rfvy * rfvy);
        rrv = Math.sqrt(rrvx * rrvx + rrvy * rrvy);

        // Determine the maximum velocity
        double max = 0.0;
        max = Math.max(lfv, lrv);
        max = Math.max(max, rfv);
        max = Math.max(max, rrv);

        // scale wheel velocities down by maximum
        // if any of them are > 1.0
        if (max > 1.0)
        {
            lfv /= max;
            lrv /= max;
            rfv /= max;
            rrv /= max;
        }
        
        // Wheel positions
        lftheta = Math.toDegrees(MathUtils.atan2(lfvx, lfvy));
        lrtheta = Math.toDegrees(MathUtils.atan2(lrvx, lrvy));
        rftheta = Math.toDegrees(MathUtils.atan2(rfvx, rfvy));
        rrtheta = Math.toDegrees(MathUtils.atan2(rrvx, rrvy));
        
        // Set outputs
        // SwerveModule handles calculating the best way to achieve these vectors
        lf.set(lfv, lftheta);
        lr.set(lrv, lrtheta);
        rf.set(rfv, rftheta);
        rr.set(rrv, rrtheta);
    }

    /**
     * Turn off the rotation PID controller.
     */
    public void disableRotatePID()
    {
        rotatePIDControl.disable();
        rotatePIDEnabled = false;
    }

    /**
     * Turn on the rotation PID controller.
     */
    public void enableRotatePID()
    {
        rotatePIDControl.enable();
        rotatePIDEnabled = true;
    }

    /**
     * Enable position control of the swerve modules.
     *
     * @todo Change to enableSteerControl(bool flag)?
     */
    public void enableSteerControl()
    {
        lf.enableSteerControl();
        rf.enableSteerControl();
        lr.enableSteerControl();
        rr.enableSteerControl();
    }

    /**
     * Get the direction the robot is facing, relative from the driver's point
     * of view.
     *
     * @return  The direction the robot is facing in degrees. This is limited
     *          from -180.0 to 180.0.
     */
    public double getDirection()
    {
        //return ((gyro.getAngle() + 360.0 + 180.0) % 360.0) - 180.0;
        return ((getHeading() + 360.0 + 180.0) % 360.0) - 180.0;
    }

    /**
     * Get the absolute heading of the robot. This is relative to the driver's
     * point of view and includes compensation for a startup offset.
     *
     * @return  The robot's heading, in degrees. This does not wrap around.
     */
    public double getHeading()
    {
        //return gyro.getAngle();
        return gyro.getAngle() + headingOffset;
    }

    /**
     *
     * @return
     */
    public double getHeadingOffset()
    {
        return headingOffset;
    }


    public boolean isRotatePIDOnTarget()
    {
        return rotatePIDControl.onTarget();
    }

    /**
     *
     */
    public void operateInit()
    {
        disableRotatePID();
        drive(0.0, 0.0, 0.0);
    }

    /**
     *
     */
    public void operateContinuous()
    {
    }

    /**
     *
     */
    public void operateExit()
    {
        disableRotatePID();
        drive(0.0, 0.0, 0.0);
    }

    /**
     *
     */
    public void operatePeriodic()
    {
        Watchdog.getInstance().feed();

        // Read joystick inputs
        double x = stick.getX();
        double y = -stick.getY();
        double z = stick.getZ();
        boolean gyroResetVal = stick.getRawButton(gyroResetButton);

        // -0 doesn't play out nicely.
        if (y == -0.0)
            y = 0.0;

        // Compute drive parameters from joystick input
        double magnitude = stick.getMagnitude() * Math.abs(stick.getMagnitude());
        double direction = Math.toDegrees(MathUtils.atan2(x, y));
        double rotation = z * z * z;
        
        if (Math.abs(magnitude) < kJoystickDeadband)
        {
            magnitude = direction = 0.0;
        }
        if (Math.abs(rotation) < kJoystickDeadband)
        {
            rotation = 0.0;

            // experimental:
            // driver motion has stopped, so turn on PID control at
            // current location.
            /*if (!rotatePIDEnabled && Math.abs(magnitude) < kJoystickDeadband)
            {
                System.out.println("[SwerveDrive] Enabling Rotation PID (Setpoint=" + getDirection() + ")");
                setRotationSetpoint(getDirection());
                enableRotatePID();
            }*/
        }
        else
        {
            // Driver wants to manually rotate. Turn off PID.
            disableRotatePID();
        }

        // Gyro reset button
        if (gyroResetButtonOld == false && gyroResetVal)
        {
            resetHeading();
        }
        gyroResetButtonOld = gyroResetVal;


        // heading offset buttons
        if (stick.getRawButton(10))
            setHeadingOffset(0.0);
        else if (stick.getRawButton(11))
            setHeadingOffset(-180.0);

        // No run, no talk. Drive. Today Crosby, today!
        if (stick.getTrigger())
        {
            // Bump driving mode when trigger is pressed.
            // TODO: This button should be configurable.
            bumpDrive(y);
        }
        else
        {
            if (rotatePIDEnabled)
                rotatePIDDrive(magnitude, direction);
            else
                drive(magnitude, direction, (stick.getRawButton(2)) ? rotation : 0.0);

            // maybe: drive(magnitude, direction, (stick.getRawButton(2)) ? rotation : 0.0);
        }

        Watchdog.getInstance().feed();
    }
    
    public void packDashboard(Dashboard dash)
    {

    }

    public void resetHeading()
    {
        System.out.println("[SwerveDrive] Heading Reset...");
        gyro.reset();
    }

    /**
     * Drive the robot using the rotation PID control.
     *
     * @param magnitude
     * @param direction
     */
    public void rotatePIDDrive(double magnitude, double direction)
    {
        System.out.println("[SwerveDrive] rotatePIDDrive - Output = " + rotatePIDOutput.get());
        drive(magnitude, direction, (rotatePIDEnabled && !rotatePIDControl.onTarget()) ? rotatePIDOutput.get() : 0.0);
    }

    /**
     *
     * @param offset
     */
    public void setHeadingOffset(double offset)
    {
        headingOffset = offset;
    }

    public void setRotationSetpoint(double value)
    {
        rotatePIDControl.setSetpoint(value);
    }

    /**
     * Long method name, I know. Todo: come up with a better name.
     * @param value
     */
    public void setRelativeRotationSetpoint(double value)
    {
        // looks confusing, but it needs to be limited from -180.0 to +180.0.
        double setpoint = ((getDirection() + value + 360.0 + 180.0) % 360.0) - 180.0;
        setRotationSetpoint(setpoint);

        System.out.println("[SwerveDrive] Relative Setpoint Set: " + value + " => " + setpoint);
    }
}