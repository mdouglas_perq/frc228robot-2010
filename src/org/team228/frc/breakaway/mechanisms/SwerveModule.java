/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.mechanisms;

import edu.wpi.first.addons.CANJaguar;
import edu.wpi.first.wpilibj.SensorBase;

/**
 * This class represents a swerve drive module.
 * It handles all of the control necessary to command
 * a module to a set position and wheel speed.
 *
 * @author Matthew Douglas
 */
public class SwerveModule extends SensorBase
{
    private CANJaguar steer, drive;

    private boolean invertDrive = false;

    double center = 0.5;        // Center point, in pot values.
                                // Ten turn pot (should) be 0.0-10.0...
    double positionMin = 0.3;   // maximum position in pot turns
    double positionMax = 0.7;   // maximum position in pot turns

    private int _steerID = 0;

    double sensitivity = -0.001111111111111;

    public SwerveModule(int steerID, int driveID)
    {
        steer = new CANJaguar(steerID, CANJaguar.ControlMode.kPosition);
        drive = new CANJaguar(driveID, CANJaguar.ControlMode.kPercentVoltage);

        steer.setPositionReference(CANJaguar.PositionReference.kPotentiometer);
        steer.configEncoderCodesPerRev((short)360);
        steer.configPotentiometerTurns((short)1);
        steer.configNeutralMode(CANJaguar.NeutralMode.kCoast);

        setPositionLimits(positionMin, positionMax);
        _steerID = steerID;
        //enableSteerControl();
        //set(0, 0);
    }

    /**
     * Disable the PID controller on the CANJaguar.
     * Effectively, steering is disabled.
     */
    public void disableSteerControl()
    {
        steer.disableControl();
    }

    /**
     * Enable the PID controller on the CANJaguar.
     * This enables position control for steering.
     */
    public void enableSteerControl()
    {
        steer.enableControl(0);
        //set(0, 0);
    }

    /**
     * Get the Proportional gain of the steering PID controller.
     *
     * @return
     */
    public double getP()
    {
        return steer.getP();
    }

    /**
     * Get the Integral gain of the steering PID controller.
     *
     * @return
     */
    public double getI()
    {
        return steer.getI();
    }

    /**
     * Get the Differential gain of the steering PID controller.
     *
     * @return
     */
    public double getD()
    {
        return steer.getD();
    }

    /**
     * Get the angular position of the module.
     *
     * @todo   Check that this is implemented properly.
     * @return The position of the module, in degrees. The range
     *         should be from -180.0 for CCW, 0.0 for straight forward,
     *         and 180.0 for CW.
     */
    public double getPosition()
    {
        return (steer.getPosition() - center) / sensitivity;
    }

    /**
     * Get the speed of the drive wheel.
     *
     * @return  The last speed set to the drive motor. Range: -1.0 to 1.0.
     */
    public double getSpeed()
    {
        return drive.get();
    }

    /**
     * Calibrate the center position of the module.
     *
     * @param value The position value (number of pot turns) at the center
     *              position facing 0 degrees forward.
     */
    public void setCenter(double value)
    {
        center = value;
    }

    /**
     * Invert the output to the drive motor.
     *
     * @param flag  True if the drive needs to be inverted, false otherwise.
     */
    public void setInvertedDrive(boolean flag)
    {
        invertDrive = flag;
    }

    /**
     * Set PID gains for steering.
     *
     * @param kp
     * @param ki
     * @param kd
     */
    public void setPID(double kp, double ki, double kd)
    {
        steer.setPID(kp, ki, kd);
    }

    /**
     * Set soft limits for the rotation of the module.
     *
     * @param min
     * @param max
     */
    public void setPositionLimits(double fwd, double rev)
    {
        positionMax = fwd;
        positionMin = rev;

        steer.configSoftPositionLimits(positionMax, positionMin);
    }

    public void setSensitivity(double value)
    {
        sensitivity = value;
    }

    /**
     * Set the desired speed and direction of the module.
     * 
     * @param magnitude The speed to send to the drive motor. Range: 0.0 - 1.0.
     * @param direction The desired direction of the module in degrees. This is
     *                  a direction relative to the robot. -180.0 for full CCW,
     *                  0.0 for straight ahead, and 180.0 for full CW.
     */
    public void set(double magnitude, double direction)
    {
        // if current position >= 0, see if direction+180 is closer
        // if current position < 0,  see if direction-180 is closer

        // I don't like -0...
        if (magnitude == -0.0)
            magnitude = 0.0;
        if (direction == -0.0)
            direction = 0.0;

        // Calculate inverted position
        double inverted = (direction >= 0.0) ? direction - 180.0 : direction + 180.0;

        // Calculate distances to possible positions
        double invertError = Math.abs(getPosition() - inverted);
        double error = Math.abs(getPosition() - direction);

        double steerValue = 0.0;

        if (Math.abs(inverted) <= 180.0 && invertError < error)
        {
            steerValue = inverted * sensitivity + center;
            drive.set((invertDrive) ? magnitude : (-1.0 * magnitude));
        }
        else
        {
            steerValue = direction * sensitivity + center;
            drive.set((invertDrive) ? (-1.0 * magnitude) : magnitude);
        }

        // Turn off steering if we're within some tolerance.
        // This is to prevent overuse of the motors.
        if (Math.abs(steer.getPosition() - steerValue) <= 0.007777) //0.005555)
        {
            disableSteerControl();
        }
        else
        {
            enableSteerControl();
            steer.set(steerValue);

            System.out.println("[SwerveModule] Module #" + _steerID + " at " + steer.getPosition() + ", moving to " + steerValue);
        }
    }
}
