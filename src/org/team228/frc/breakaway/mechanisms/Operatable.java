/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.mechanisms;

/**
 *
 * @author Matthew Douglas
 */
public interface Operatable
{
    public void operateContinuous();
    public void operateInit();
    public void operatePeriodic();
    public void operateExit();
}
