/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.mechanisms;

import edu.wpi.first.addons.CANJaguar;
import edu.wpi.first.wpilibj.Dashboard;
import edu.wpi.first.wpilibj.Joystick;

import org.team228.frc.breakaway.util.Config;
import org.team228.frc.breakaway.dashboard.DashboardDataProvider;

/**
 *
 * @author Matt
 */
public class Vacuum implements Operatable, DashboardDataProvider
{
    private static volatile Vacuum instance;

    private CANJaguar low;
    private CANJaguar high;
    private Joystick gamepad;

    private int highButton = 5;
    private int lowButton = 6;

    /**
     *
     * @return
     */
    public static Vacuum getInstance()
    {
        if (instance == null)
        {
            synchronized (Vacuum.class)
            {
                if (instance == null)
                {
                    instance = new Vacuum();
                }
            }
        }

        return instance;
    }

    /**
     *
     */
    private Vacuum()
    {
        System.out.println("[Vacuum] Initializing...");
        
        high = new CANJaguar(Config.getInt("vacuum.high.canid", 29));
        low = new CANJaguar(Config.getInt("vacuum.low.canid", 28));

        high.setVoltageRampRate(Config.getDouble("vacuum.high.ramprate", 24.0));
        low.setVoltageRampRate(Config.getDouble("vacuum.low.ramprate", 24.0));

        gamepad = new Joystick(Config.getInt("vacuum.joystick", 2));

        highButton = Config.getInt("vacuum.high.button", 5);
        lowButton = Config.getInt("vacuum.low.button", 6);
    }

    /**
     * @todo Implement
     * @return
     */
    public boolean hasBallHigh()
    {
        //return false;
        // todo: config values, fine tune
        return (high.getOutputCurrent() > 6.0 && high.getOutputCurrent() < 8.0);
    }

    /**
     * @todo Implement
     * @return
     */
    public boolean hasBallLow()
    {
        //return false;
        return (high.getOutputCurrent() > 6.0 && high.getOutputCurrent() < 8.0);
    }


    /**
     *
     * @param on
     */
    public void setHigh(boolean on)
    {
        high.set((on) ? 1.0 : 0.0);
    }

    /**
     *
     * @param on
     */
    public void setLow(boolean on)
    {
        low.set((on) ? 1.0 : 0.0);
    }

    /**
     *
     */
    public void operateInit()
    {
        setHigh(false);
        setLow(false);
    }

    /**
     *
     */
    public void operateContinuous()
    {
    }

    /**
     *
     */
    public void operatePeriodic()
    {
        setHigh(gamepad.getRawButton(highButton));
        setLow(gamepad.getRawButton(lowButton));
    }

    /**
     *
     */
    public void operateExit()
    {
        setHigh(false);
        setLow(false);
    }

    public void packDashboard(Dashboard dash)
    {

    }
}
