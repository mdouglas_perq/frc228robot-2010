/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.autonomous;

/**
 *
 */
public class MidfieldRoutine extends ActionSequence
{
    public MidfieldRoutine()
    {
        // Shift kicker low
        add(Action.kickerShiftLow());

        // Load the kicker
        add(Action.kickerLoad());

        // Turn on the vacuum
        add(Action.vacuumLow(true));

        // Wait for the vacuum to ramp up
        add(Action.wait(0.3));

        // Drive toward balls, 0.5 sec
        //add(Action.drive(0.4, -180.0, 0.0, 0.75));
        add(Action.bumpDrive(-0.4, 0.5));

        // Kick
        add(Action.kick());

        // Reload the kicker
        add(Action.kickerLoad());

        // Turn on vacuum
        add(Action.vacuumLow(true));

        // Wait for the vacuum to ramp up
        add(Action.wait(0.3));

        // Drive toward next ball
        //add(Action.drive(0.4, -180.0, 0.0, 1.75));
        add(Action.bumpDrive(-0.4, 1.0));

        // Kick
        add(Action.kick());

        // Turn off vacuum
        add(Action.vacuumLow(false));

        // Get out of the way
        add(Action.drive(0.3, -90.0, 0.0, 1.75));

        // Done.
        add(Action.stop());
    }
}
