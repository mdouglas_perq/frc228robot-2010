/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.autonomous;

import java.util.Vector;

/**
 * Represents a sequence of Actions to perform in an
 * Autonomous routine.
 */
public class ActionSequence implements AutonomousRoutine
{

    private Action currentAction = null;
    private Vector sequence = new Vector();
    private boolean complete = false;
    private int step = 0;

    /**
     * Add an Action to the sequence.
     *
     * @param act   The action to add.
     */
    public void add(Action act)
    {
        sequence.addElement(act);
    }

    /**
     * Initialize the sequence.
     */
    public void init()
    {
        step = 0;
        complete = false;
        loadStep();
    }

    /**
     * Load the current step.
     */
    private void loadStep()
    {
        if (step < sequence.size())
        {
            try
            {
                currentAction = (Action)sequence.elementAt(step);
                System.out.println("[ActionSequence] Beginning Step " + step);
                currentAction.init();
            }
            catch (Exception e)
            {
                currentAction = null;
                e.printStackTrace();
            }
        }
        else
        {
            currentAction = null;
            complete = true;

            System.out.println("[ActionSequence] Complete!");
        }
    }

    public void exit()
    {
        complete = true;
    }

    public void continuous()
    {
        if (!complete)
        {
            if (currentAction != null)
            {
                if (currentAction.isDone())
                {
                    step++;
                    loadStep();
                }
                else
                {
                    currentAction.continuous();
                }
            }
            else
            {
                // bad step? handle this by moving on to next
                System.out.println("[ActionSequence] Bad step: " + step + ", moving on...");
                step++;
                loadStep();
            }
        }
    }

    public void periodic()
    {
        if (!complete && currentAction != null)
            currentAction.periodic();
    }

    public boolean isDone()
    {
        return complete;
    }
}
