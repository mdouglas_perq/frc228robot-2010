/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.autonomous;

/**
 *
 */
public class HomeAngledRoutine extends ActionSequence
{

    public HomeAngledRoutine()
    {
        // Shift kicker low
        add(Action.kickerShiftLow());

        // Load the kicker
        add(Action.kickerLoad());

        // Turn on the vacuum
        add(Action.vacuumLow(true));

        // Wait for the vacuum to ramp up
        add(Action.wait(0.75));

        // additional wait
        add(Action.wait(3.0));

        // Drive toward ball
        add(Action.drive(0.4, -180.0, 0.0, 1.0));

        // Rotate toward goal
        add(Action.rotate(-15.0));

        // Kick the ball
        add(Action.kick());

        // Rotate back 7 deg
        //add(Action.rotate(7.0));

        // Turn off vacuum
        add(Action.vacuumLow(false));

        // Done.
        add(Action.stop());
    }
}
