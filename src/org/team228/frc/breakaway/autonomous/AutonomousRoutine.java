/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.autonomous;

/**
 * Iterative Autonomous Routine.
 * @todo   Look into spawning Autonomous as independent task,
 *         and killing the task when autonomous mode ends.
 */
public interface AutonomousRoutine
{
    public void init();
    public void continuous();
    public void exit();
    public void periodic();
}
