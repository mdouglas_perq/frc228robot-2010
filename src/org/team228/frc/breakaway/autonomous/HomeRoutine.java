/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.autonomous;

/**
 *
 */
public class HomeRoutine extends ActionSequence
{
    public HomeRoutine()
    {
        // Shift kicker low
        add(Action.kickerShiftLow());

        // Load the kicker
        add(Action.kickerLoad());

        // Turn on the vacuum
        add(Action.vacuumLow(true));

        // Wait for the vacuum to ramp up
        add(Action.wait(0.3));

        // Additional wait
        add(Action.wait(3.0));

        // Drive toward ball
        add(Action.drive(0.4, -180.0, 0.0, 1.0));

        // Kick the ball
        add(Action.kick());

        // Turn off vacuum
        add(Action.vacuumLow(false));

        // Get out of the way
        add(Action.drive(0.3, -90.0, 0.0, 1.0));

        // Done.
        add(Action.stop());
    }
}
