/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.autonomous;

import edu.wpi.first.wpilibj.Timer;

import org.team228.frc.breakaway.mechanisms.Kicker;
import org.team228.frc.breakaway.mechanisms.SwerveDrive;
import org.team228.frc.breakaway.mechanisms.Vacuum;

/**
 * Action to be executed in an ActionSequence.
 * Provides some default Actions that can be dynamically created.
 *
 */
public abstract class Action
{
    public abstract boolean isDone();

    public abstract void init();
    public abstract void continuous();
    public abstract void periodic();

    /**
     * Create an Action that drives the robot for a specified period of time.
     *
     * @param magnitude     Drive speed, 0.0 to 1.0.
     * @param direction     Drive direction, relative to the field. -180.0 to 180.0.
     * @param rotation      Rotation value, -1.0 to 1.0.
     * @param duration      The time to drive, in seconds.
     * @return              An Action that, when executed, drives the robot
     *                      according to the parameters.
     */
    public static Action drive(final double magnitude, final double direction, final double rotation, final double duration)
    {
        return (new Action() {
                boolean done = false;
                Timer timer = new Timer();

                public boolean isDone()
                {
                    return done;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: drive");
                    done = false;
                    timer.stop();
                    timer.reset();
                    timer.start();
                    SwerveDrive.getInstance().drive(magnitude, direction, rotation);
                }

                public void continuous()
                {
                    if (timer.get() >= duration)
                    {
                        SwerveDrive.getInstance().drive(0.0, 0.0, 0.0);
                        done = true;
                        timer.stop();
                    }
                }

                public void periodic() {}
            });
    }

    public static Action bumpDrive(final double speed, final double duration)
    {
        return (new Action() {
                boolean done = false;
                Timer timer = new Timer();

                public boolean isDone()
                {
                    return done;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: drive");
                    done = false;
                    timer.stop();
                    timer.reset();
                    timer.start();
                    SwerveDrive.getInstance().bumpDrive(speed);
                }

                public void continuous()
                {
                    if (timer.get() >= duration)
                    {
                        SwerveDrive.getInstance().drive(0.0, 0.0, 0.0);
                        done = true;
                        timer.stop();
                    }
                }

                public void periodic() {}
            });
    }

    public static Action stop()
    {
        return (new Action() {
                public boolean isDone()
                {
                    return true;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: stop");
                    SwerveDrive.getInstance().drive(0.0, 0.0, 0.0);
                }

                public void periodic() { }
                public void continuous() { }
        });
    }

    // todo: kick only if a ball is possessed?
    public static Action kick()
    {
        return (new Action() {
                boolean done = false;

                public boolean isDone()
                {
                    return done;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: kick");
                    done = false;
                    Kicker.getInstance().fire();
                }

                public void continuous()
                {
                    done = Kicker.getInstance().getState() != Kicker.State.kFiring;
                }

                public void periodic() { }
        });
    }

    public static Action kickerLoad()
    {
        return (new Action() {
                boolean done = false;

                public boolean isDone()
                {
                    return done;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: kickerLoad");
                    if (!Kicker.getInstance().isArmed())
                    {
                        done = false;
                        Kicker.getInstance().fire();
                    }
                    else
                    {
                        done = true;
                    }
                }

                public void continuous()
                {
                    done = Kicker.getInstance().getState() != Kicker.State.kFiring;
                }

                public void periodic() { }
        });
    }

    public static Action kickerShiftHigh()
    {
        return (new Action() {
                boolean done = false;

                public boolean isDone()
                {
                    return done;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: kickerShiftHigh");

                    if (Kicker.getInstance().getMode() != Kicker.Mode.kHighPower)
                    {
                        done = false;
                        Kicker.getInstance().shiftHigh();
                    }
                    else
                    {
                        done = true;
                    }
                }

                public void continuous()
                {
                    if (!done)
                    {
                        done = (Kicker.getInstance().getMode() == Kicker.Mode.kHighPower);
                    }
                }

                public void periodic() { }
        });
    }

    public static Action kickerShiftLow()
    {
        return (new Action() {
                boolean done = false;

                public boolean isDone()
                {
                    return done;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: kickerShiftLow");

                    if (Kicker.getInstance().getMode() != Kicker.Mode.kLowPower)
                    {
                        done = false;
                        Kicker.getInstance().shiftLow();
                    }
                    else
                    {
                        done = true;
                    }
                }

                public void continuous()
                {
                    if (!done)
                    {
                        done = (Kicker.getInstance().getMode() == Kicker.Mode.kLowPower);
                    }
                }

                public void periodic() { }
        });
    }

    // default timeout of 5.0s...
    public static Action rotate(final double amount)
    {
        return (new Action() {
                boolean done = false;
                Timer timer = new Timer();

                public boolean isDone()
                {
                    return done;
                }
                public void init() 
                {
                    System.out.println("[Action] Executing: rotate");

                    done = false;
                    timer.stop();
                    timer.reset();
                    timer.start();
                    SwerveDrive.getInstance().setRelativeRotationSetpoint(amount);
                    SwerveDrive.getInstance().enableRotatePID();
                }
                public void continuous() { }
                public void periodic() 
                {   
                    if (SwerveDrive.getInstance().isRotatePIDOnTarget() || timer.get() >= 5.0)
                    {
                        SwerveDrive.getInstance().disableRotatePID();
                        SwerveDrive.getInstance().drive(0.0, 0.0, 0.0);
                        done = true;
                        timer.stop();
                    }
                    else
                    {
                        SwerveDrive.getInstance().rotatePIDDrive(0.0, 0.0);
                    }
                }

        });
    }
    public static Action vacuumHigh(final boolean value)
    {
        return (new Action() {

                public boolean isDone()
                {
                    return true;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: vacuumHigh");
                    Vacuum.getInstance().setHigh(value);
                }

                public void continuous() { }
                public void periodic() { }
        });
    }

    public static Action vacuumLow(final boolean value)
    {
        return (new Action() {

                public boolean isDone()
                {
                    return true;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: vacuumLow");
                    Vacuum.getInstance().setLow(value);
                }

                public void continuous() { }
                public void periodic() { }
        });
    }

    public static Action wait(final double duration)
    {
        return (new Action() {
                boolean done = false;
                Timer timer = new Timer();

                public boolean isDone()
                {
                    return done;
                }

                public void init()
                {
                    System.out.println("[Action] Executing: wait");

                    done = false;
                    timer.stop();
                    timer.reset();
                    timer.start();
                }

                public void continuous()
                {
                    if (timer.get() >= duration)
                    {
                        done = true;
                        timer.stop();
                    }
                }

                public void periodic() { }
        });
    }
}
