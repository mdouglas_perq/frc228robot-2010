/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.autonomous;

/**
 *
 */
public class MidfieldAngledRoutine extends ActionSequence
{

    public MidfieldAngledRoutine()
    {
        // Shift kicker low
        add(Action.kickerShiftLow());

        // Load the kicker
        add(Action.kickerLoad());

        // Turn on the vacuum
        add(Action.vacuumLow(true));

        // Wait for the vacuum to ramp up
        add(Action.wait(0.3));
        
        // Drive toward balls, 0.5 sec
       // add(Action.drive(0.4, -180.0, 0.0, 0.75));
        add(Action.bumpDrive(-0.4, 0.5));
        
        add(Action.wait(0.5));

        // Rotate toward goal
        add(Action.rotate(-20.0));

        // Kick
        add(Action.kick());

        // Reload the kicker
        add(Action.kickerLoad());

        // Rotate back
        add(Action.rotate(20.0));

        // Turn on the vacuum
        add(Action.vacuumLow(true));

        // Wait for the vacuum to ramp up
        add(Action.wait(0.3));

        // Drive toward next ball
        //add(Action.drive(0.4, -180.0, 0.0, 1.75));
        add(Action.bumpDrive(-0.4, 1.0));

        add(Action.wait(0.5));

        // Rotate toward goal
        add(Action.rotate(-20.0));

        // Kick
        add(Action.kick());

        // Turn off vacuum
        add(Action.vacuumLow(false));

        // Done.
        add(Action.stop());
    }
}
