
package org.team228.frc.breakaway.autonomous;

/**
 *
 */
public class BackRoutine extends ActionSequence
{
    public BackRoutine()
    {
        // Shift kicker high
        add(Action.kickerShiftHigh());

        // Load the kicker
        add(Action.kickerLoad());

        // Turn on the vacuum
        add(Action.vacuumHigh(true));

        // Wait for the vacuum to ramp up
        add(Action.wait(0.3));

        // Drive toward balls, 0.5 sec
        //add(Action.drive(0.4, 180.0, 0.0, 0.75));
        add(Action.bumpDrive(0.4, 0.75));

        // Kick
        add(Action.kick());

        // Reload the kicker
        add(Action.kickerLoad());

        // Turn on vacuum
        add(Action.vacuumHigh(true));

        // Wait for the vacuum to ramp up
        add(Action.wait(0.3));

        // Drive toward next ball, 2.5 sec
        //add(Action.drive(0.4, 180.0, 0.0, 1.0));
        add(Action.bumpDrive(0.4, 1.0));

        // Kick
        add(Action.kick());

        // Reload the kicker
        add(Action.kickerLoad());

        // Turn on vacuum
        add(Action.vacuumHigh(true));

        // Wait for the vacuum to ramp up
        add(Action.wait(0.3));

        // Drive toward next ball, 2.5 sec
       // add(Action.drive(0.4, 180.0, 0.0, 1.0));
        add(Action.bumpDrive(0.4, 1.0));

        // Kick
        add(Action.kick());

        add(Action.kick());

        // Turn off vacuum
        add(Action.vacuumHigh(false));

        // Done.
        add(Action.stop());
    }
}
