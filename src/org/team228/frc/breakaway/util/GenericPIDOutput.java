/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.util;

import edu.wpi.first.wpilibj.PIDOutput;

/**
 *
 * @author Matt
 */
public class GenericPIDOutput implements PIDOutput
{
    double value = 0.0;

    public double get()
    {
        return value;
    }

    public void pidWrite(double output)
    {
        value = output;
    }

}
