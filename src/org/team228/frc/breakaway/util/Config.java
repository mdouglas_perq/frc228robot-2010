/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.util;

import com.sun.squawk.microedition.io.FileConnection;

import com.sun.squawk.util.LineReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;
import javax.microedition.io.Connector;
import javax.microedition.io.Connection;

/**
 * Static class for reading configuration data.
 * The configuration information is stored in a text file
 * at /ni-rt/system/config.txt.
 *
 */
public class Config
{
    private static Hashtable values = new Hashtable();

    private static boolean loaded = false; //Config.load();

    /**
     * Read the configuration file and load the information.
     * This should be called very early on in the robot program.
     */
    public static boolean load()
    {
        System.out.println("[Config] Attempting to load configuration file....");

        try
        {
            // Clear any existing values
            values.clear();
           
            // Open input stream
            InputStream is = Connector.openInputStream("file://config.txt");
            LineReader lr = new LineReader(new InputStreamReader(is));

            // Read the file, line by line
            String line = null;
            while ((line = lr.readLine()) != null)
            {
                int m = line.indexOf("=");
                if (m != -1)
                {
                    String key = line.substring(0, m - 1).trim();
                    String value = line.substring(m + 2, line.length()).trim();

                    // Store the configuration value
                    values.put(key, value);

                    System.out.println("[Config] Loaded: '" + key + "' = '" + value + "'");
                }
            }

            // Close the file
            is.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        System.out.println("[Config] Configuration loaded!");

        return true;
    }

    /**
     * Read a boolean configuration value.
     *
     * @param key           The configuration key.
     * @return              The configuration value. Defaults to
     *                      <code>false</code> if the key is not found.
     */
    public static boolean getBoolean(String key)
    {
        return getBoolean(key, false);
    }

    /**
     * Read a boolean configuration value.
     *
     * @param key           The configuration key.
     * @param defaultValue  The default value to return if the key is not found.
     * @return              The configuration value if found, and
     *                      <code>defaultValue</code> otherwise.
     */
    public static boolean getBoolean(String key, boolean defaultValue)
    {
        if (values.containsKey(key))
            return getString(key).equalsIgnoreCase("true");
        else
            return defaultValue;
    }

    /**
     * Read a double configuration value.
     *
     * @param key           The configuration key.
     * @return              The configuration value. Defaults to 0.0 if the key
     *                      is not found or contains a bad value.
     */
    public static double getDouble(String key)
    {
        return getDouble(key, 0.0);
    }

    /**
     * Read a double configuration value.
     *
     * @param key           The configuration key.
     * @param defaultValue  The default value to return if the key is not found
     *                      or if it contains a bad value.
     * @return              The configuration value if found and valid, or
     *                      <code>defaultValue</code> otherwise.
     */
    public static double getDouble(String key, double defaultValue)
    {
        try
        {
            return Double.parseDouble(getString(key));
        }
        catch (NumberFormatException e)
        {
            e.printStackTrace();
            return defaultValue;
        }
    }

    /**
     * Read an integer configuration value.
     *
     * @param key           The configuration key.
     * @return              The configuration value. Defaults to 0 if the key
     *                      is not found or contains a bad value.
     */
    public static int getInt(String key)
    {
        return getInt(key, 0);
    }

    /**
     * Read an integer configuration value.
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public static int getInt(String key, int defaultValue)
    {
        try
        {
            return Integer.parseInt(getString(key));
        }
        catch (NumberFormatException e)
        {
            e.printStackTrace();
            return defaultValue;
        }
    }

    /**
     * Read a long integer configuration value.
     *
     * @param key
     * @return
     */
    public static long getLong(String key)
    {
        return getLong(key, 0L);
    }

    /**
     * Read a long integer configuration value.
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public static long getLong(String key, long defaultValue)
    {
        try
        {
            return Long.parseLong(getString(key));
        }
        catch (NumberFormatException e)
        {
            e.printStackTrace();
            return defaultValue;
        }
    }

    /**
     * Read a string configuration value.
     *
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getString(String key, String defaultValue)
    {
        if (values.containsKey(key))
            return (String)values.get(key);
        else
            return defaultValue;
    }

    /**
     * Read a string configuration value.
     * 
     * @param key
     * @return
     */
    public static String getString(String key)
    {
        return getString(key, "");
    }
}
