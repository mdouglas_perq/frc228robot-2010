/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.devices;

import edu.wpi.first.wpilibj.DigitalModule;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.SensorBase;

/**
 * Class used to interface with a New Haven LCD Display.
 * The New Haven LCD display is a 4x20 display operated on the
 * I2C bus.
 *
 * @author Matthew Douglas
 */
public class NewHavenLCD extends SensorBase
{
    public static final int kNumRows = 4;
    public static final int kNumCols = 20;

    public static final int kI2CAddress = 0x50;
    public static final int kCommandPrefix = 0xFE;

    // TODO: Verify these.
    // These are values from 2009.
    // Docs state 0x00, 0x40, 0x14, 0x54.
    public static final int kLine1 = 0x00,
                            kLine2 = 0x28,
                            kLine3 = 0x14,
                            kLine4 = 0x3C;

    // Commands
    public static final int kDisplayOn = 0x41,
                            kDisplayOff = 0x42,
                            kSetCursor = 0x45,
                            kCursorHome = 0x46,
                            kUnderlineOn = 0x47,
                            kUnderlineOff = 0x48,
                            kCursorLeft = 0x49,
                            kCursorRight = 0x4A,
                            kBlinkingOn = 0x4B,
                            kBlinkingOff = 0x4C,
                            kBackspace = 0x4E,
                            kClear = 0x51,
                            kContrast = 0x52,
                            kBrightness = 0x53,
                            kCustomChar = 0x54,
                            kShiftRight = 0x55,
                            kShiftLeft = 0x56,
                            kChangeI2CAddress = 0x62,
                            kDisplayFwVersion = 0x70,
                            kDisplayI2CAddress = 0x72;


    private I2C i2c;

    public NewHavenLCD()
    {
        this(getDefaultDigitalModule());
    }

    /**
     * Constructor.
     *
     * @param slot  The Digital slot to which the display is attached.
     */
    public NewHavenLCD(int slot)
    {
        DigitalModule module = DigitalModule.getInstance(slot);
        i2c = module.getI2C(kI2CAddress);
    }

    /**
     * Move the cursor back one space and delete a character.
     */
    public void backspace()
    {
        sendCommand(kBackspace);
    }

    /**
     * Set the blinking cursor.
     *
     * @param on    <code>true</code> to turn on.
     */
    public void blink(boolean on)
    {
        sendCommand((on) ? kBlinkingOn : kBlinkingOff);
    }

    /**
     * Clear the text from the display.
     */
    public void clear()
    {
        sendCommand(kClear);
    }

    /**
     * Turn the display off.
     */
    public void disable()
    {
        sendCommand(kDisplayOff);
    }

    /**
     * Write the firmware version on the display.
     */
    public void displayFwVersion()
    {
        sendCommand(kDisplayFwVersion);
    }

    /**
     * Write the I2C address on the display.
     */
    public void displayI2CAddress()
    {
        sendCommand(kDisplayI2CAddress);
    }

    /**
     * Turn the display on.
     */
    public void enable()
    {
        sendCommand(kDisplayOn);
    }

    /**
     * Set the cursor home.
     * This places the cursor at the top left corner.
     */
    public void home()
    {
        sendCommand(kCursorHome);
    }

    /**
     * Move the cursor one space to the left.
     */
    public void left()
    {
        sendCommand(kCursorLeft);
    }

    /**
     * Load a custom character.
     *
     * This is not yet implemented.
     *
     * @todo  Implement
     *
     * @param address
     * @param bitmap
     */
    public void loadCustomCharacter(int address, byte[] bitmap)
    {
    }

    /**
     * Move the cursor one space to the right.
     */
    public void right()
    {
        sendCommand(kCursorRight);
    }

    /**
     * Set the brightness of the display.
     *
     * @param value Value between 1 and 8.
     *              8 is the highest.
     *              Default value is 1.
     */
    public void setBrightness(int value)
    {
        // Syntax: 0xF3 0x53 [brightness]
        // Value from 1 to 8. Default is 1.
        if (value < 1 || value > 8)
            return;

        sendCommand(kBrightness, value);
    }

    /**
     * Set the contrast of the display.
     *
     * @param value Value between 1 and 50.
     *              50 is the highest.
     *              Default is 40.
     */
    public void setContrast(int value)
    {
        // Syntax: 0xFE 0x53 [contrast]
        if (value < 1 || value > 50)
            return;

        sendCommand(kContrast, value);
    }

    /**
     * Set the cursor to a specific position.
     *
     * @param pos   Move the cursor to this position.
     */
    public void setCursor(int pos)
    {
        sendCommand(kSetCursor, pos);
    }

    /**
     * Shift the text on the display one place to the left.
     */
    public void shiftLeft()
    {
        sendCommand(kShiftLeft);
    }

    /**
     * Shift the text on the display one place to the right.
     */
    public void shiftRight()
    {
        sendCommand(kShiftRight);
    }

    /**
     * Toggle the underline cursor.
     *
     * @param on
     */
    public void underline(boolean on)
    {
        sendCommand((on) ? kUnderlineOn : kUnderlineOff);
    }

    public synchronized void send(byte[] buffer)
    {
        i2c.transaction(buffer, buffer.length, null, 0);
    }

    /**
     * Send a command to the display.
     *
     * @param cmd   The command to send, excluding the
     *              standard <code>0xFE</code> prefix.
     */
    public synchronized void sendCommand(final int cmd)
    {
        byte[] buffer = new byte[2];
        buffer[0] = (byte)kCommandPrefix;
        buffer[1] = (byte)cmd;

        i2c.transaction(buffer, buffer.length, null, 0);
    }

    /**
     * Send a parameterized command to the display.
     *
     * @param cmd   The command to send.
     * @param param
     */
    public synchronized void sendCommand(final int cmd, int param)
    {
        byte[] buffer = new byte[3];
        buffer[0] = (byte)kCommandPrefix;
        buffer[1] = (byte)cmd;
        buffer[2] = (byte)param;

        i2c.transaction(buffer, buffer.length, null, 0);
    }

    /**
     * Send a parameterized command to the display.
     *
     * @param cmd   The command to send.
     * @param param
     * @param param2
     */
    public synchronized void sendCommand(final int cmd, int param, int param2)
    {
        byte[] buffer = new byte[4];
        buffer[0] = (byte)kCommandPrefix;
        buffer[1] = (byte)cmd;
        buffer[2] = (byte)param;
        buffer[3] = (byte)param2;

        i2c.transaction(buffer, buffer.length, null, 0);
    }

    /**
     * Send a parameterized command to the display.
     *
     * @param cmd   The command to send.
     * @param param
     * @param param2
     * @param param3
     */
    public synchronized void sendCommand(final int cmd, int param, int param2, int param3)
    {
        byte[] buffer = new byte[5];
        buffer[0] = (byte)kCommandPrefix;
        buffer[1] = (byte)cmd;
        buffer[2] = (byte)param;
        buffer[3] = (byte)param2;
        buffer[4] = (byte)param3;

        i2c.transaction(buffer, buffer.length, null, 0);
    }

    /**
     * Send a parameterized command to the display.
     *
     * @param cmd   The command to send.
     * @param param
     * @param param2
     * @param param3
     * @param param4
     */
    public synchronized void sendCommand(final int cmd, int param, int param2, int param3, int param4)
    {
        byte[] buffer = new byte[6];
        buffer[0] = (byte)kCommandPrefix;
        buffer[1] = (byte)cmd;
        buffer[2] = (byte)param;
        buffer[3] = (byte)param2;
        buffer[4] = (byte)param3;
        buffer[5] = (byte)param4;

        i2c.transaction(buffer, buffer.length, null, 0);
    }

    /**
     * Write a string of text to the display.
     *
     * @param pos   The position to place the text.
     * @param text  The text to write.
     */
    public void write(int pos, String text)
    {
        setCursor(pos);
        write(text);
    }

    /**
     * Write a string of text to the display.
     * The text is written starting from the current cursor location.
     *
     * @param text  The text to write.
     */
    public synchronized void write(String text)
    {
        byte[] bytes;
        
        try
        {
            bytes = text.getBytes("ASCII");
        }
        catch (java.io.UnsupportedEncodingException ex)
        {
            bytes = text.getBytes();    // Use default encoding
            ex.printStackTrace();
        }

        for (int i = 0; i < bytes.length;)
        {
            int remaining = bytes.length - i;
            if (remaining > 6) remaining = 6;   // Maximum of 6 bytes at once
            byte[] buffer = new byte[remaining];
            for (int j = 0; j < remaining; j++)
                buffer[j] = bytes[i++];

            send(buffer);
        }
    }
}
