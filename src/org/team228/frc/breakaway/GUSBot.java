/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway;

import org.team228.frc.breakaway.util.Config;
import org.team228.frc.breakaway.mechanisms.Vacuum;
import org.team228.frc.breakaway.mechanisms.SwerveDrive;
import org.team228.frc.breakaway.mechanisms.Kicker;
import org.team228.frc.breakaway.autonomous.*;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.IterativeRobot;
import edu.wpi.first.wpilibj.Timer;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class GUSBot extends IterativeRobot
{
    private DriverStation ds;

    private SwerveDrive swerve;
    private Kicker kicker;
    private Vacuum vacuum;
    private VisionTracker vision;

    private AutonomousRoutine autoRoutine = null;

    private int autonomousMode = 0;

    /**
     * This function is run when the robot is first started up and should be
     * used for any initialization code.
     */
    public void robotInit()
    {
        getWatchdog().setEnabled(false);

        // Load configuration
        Config.load();

        // Driver station
        ds = DriverStation.getInstance();

        // Wait a small delay for the camera to boot up
        System.out.println("[GUSBot] Waiting for camera...");
        Timer.delay(4.0);

        // Start the vision tracking task
        System.out.println("[GUSBot] Starting vision task...");
        vision = VisionTracker.getInstance();
        vision.enable();

        // Create mechanisms
        swerve = SwerveDrive.getInstance();
        kicker = Kicker.getInstance();
        vacuum = Vacuum.getInstance();

        // Turn on the Watchdog
        getWatchdog().setExpiration(0.5);
        getWatchdog().setEnabled(true);

        System.out.println("[GUSBot] Ready!");
    }

    /**
     * This method is called when the robot enters disabled mode.
     */
    public void disabledInit()
    {
        swerve.operateExit();
        kicker.operateExit();
        vacuum.operateExit();
    }

    /**
     * This method is called periodically while the robot is disabled.
     */
    public void disabledPeriodic()
    {
        getWatchdog().feed();
    }

    /**
     * This method is called when the robot enters autonomous mode.
     *
     * Number 228 not just robot, number 228 is alive!
     */
    public void autonomousInit()
    {
        swerve.operateExit();
        kicker.operateExit();
        vacuum.operateExit();

         // Get autonomous selection
        // By default, DIO is HIGH, so turning OFF (LOW) will enable.
        boolean modeBit0 = !ds.getDigitalIn(1);
        boolean modeBit1 = !ds.getDigitalIn(2);

        boolean modeBit7 = !ds.getDigitalIn(8);

        int mode = 0;
        if (modeBit0) mode += 1;
        if (modeBit1) mode += 2;
        autonomousMode = mode;

        // Select AutonomousRoutine based on mode
        // Mode# | DIO #1 | DIO #2 | Description
        //-------+--------+--------+------------------------
        //     0 | ON     | ON     | Do nothing.
        //     1 | OFF    | ON     | Home zone.
        //     2 | ON     | OFF    | Middle zone.
        //     3 | OFF    | OFF    | Back zone.
        //
        // For home and middle zone, if DIO #7 is OFF, use an
        // angled routine.
        // Basically, turn off DIO #1 for Home. Turn off DIO #2 for middle.
        // Turn off both #1 and #2 for far. Turn off #8 when using angled.
        switch (autonomousMode)
        {
            case 0:
                swerve.setHeadingOffset(0.0);
                autoRoutine = null;
                break;
            case 1:
                swerve.setHeadingOffset(0.0);
                if (modeBit7)
                    autoRoutine = new HomeAngledRoutine();
                else
                    autoRoutine = new HomeRoutine();
                break;
            case 2:
                swerve.setHeadingOffset(0.0);
                if (modeBit7)
                    autoRoutine = new MidfieldAngledRoutine();
                else
                    autoRoutine = new MidfieldRoutine();
                break;
            case 3:
                // This one requires that the robot is set up backwards
                swerve.setHeadingOffset(-180.0);
                autoRoutine = new BackRoutine();
                break;
            default:
                swerve.setHeadingOffset(0.0);
                autoRoutine = null;
                break;
        }

        System.out.println("[GUSBot] Auto Mode: " + autonomousMode);

        if (autoRoutine != null)
            autoRoutine.init();
    }

    public void autonomousContinuous()
    {
        if (autoRoutine != null)
            autoRoutine.continuous();
    }

    /**
     * This method is called periodically during autonomous.
     * It is synchronized with the arrival of packets from the
     * DriverStation at ~50Hz.
     */
    public void autonomousPeriodic()
    {
        getWatchdog().feed();

        if (autoRoutine != null)
            autoRoutine.periodic();
    }

    /**
     * This method is called continuously during teleop mode.
     */
    public void teleopContinuous()
    {
        swerve.operateContinuous();
        kicker.operateContinuous();
        vacuum.operateContinuous();
    }

    /**
     * This method is called when the robot enters teleop mode.
     */
    public void teleopInit()
    {
        swerve.operateInit();
        kicker.operateInit();
        vacuum.operateInit();
    }

    /**
     * This method is called periodically during teleop mode.
     * It is synchronized with the arrival of packets from the
     * DriverStation at ~50Hz. Any <code>Operatble</code> object
     * that is driven using DS inputs should be called here.
     */
    public void teleopPeriodic()
    {
        getWatchdog().feed();

        swerve.operatePeriodic();
        kicker.operatePeriodic();
        vacuum.operatePeriodic();
    }
}
