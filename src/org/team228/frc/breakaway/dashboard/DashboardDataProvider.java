/*******************************************************************************
 * GUS Robotics Team, Inc.
 * FIRST Robotics Competition 2010
 * FRC Team 228 Breakaway Robot Control Software
 ******************************************************************************/

package org.team228.frc.breakaway.dashboard;

import edu.wpi.first.wpilibj.Dashboard;

/**
 * Interface for objects that provide feedback to the Dashboard.
 */
public interface DashboardDataProvider
{
    public void packDashboard(Dashboard dash);
}
